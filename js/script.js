/* Author: kaique.developer@gmail.com */

$(function () {
	SimplePress.start({
		holder: {
			width: '100%',
			height: '600px',
		},
		slide: { 
			width: '960px',
			height: '550px',
			spaceBetween: 90,
		}
	});
});
